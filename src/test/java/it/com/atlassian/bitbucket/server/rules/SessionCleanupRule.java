package it.com.atlassian.bitbucket.server.rules;

import org.junit.Ignore;
import org.junit.rules.ExternalResource;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Ignore
public class SessionCleanupRule extends ExternalResource {

    private final com.atlassian.webdriver.testing.rule.SessionCleanupRule delegate =
            new com.atlassian.webdriver.testing.rule.SessionCleanupRule();

    @Override
    public Statement apply(final Statement base, Description description) {
        if (description.getTestClass().isAnnotationPresent(KeepSession.class)) {
            return skipRule(base);
        } else {
            return delegate.apply(base, description);
        }
    }

    private Statement skipRule(final Statement base) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                base.evaluate();
            }
        };
    }

    @Retention(RetentionPolicy.RUNTIME)
    @Target({ElementType.TYPE})
    public @interface KeepSession {
    }

}
