package com.atlassian.bitbucket.server.spotify.feature.client.model;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "track")
@XmlAccessorType(XmlAccessType.FIELD)
public class Track extends SpotifyModel
{
    @XmlElement(name="album")
    private final Album album;

    @XmlElement(name="artists")
    private final Artist[] artists;

    public Track(final String href, final String name, final Album album, final Artist[] artists)
    {
        super(href, name);
        this.album = album;
        this.artists = artists;
    }

    public Album getAlbum()
    {
        return album;
    }

    public Artist[] getArtists()
    {
        return artists;
    }

}
