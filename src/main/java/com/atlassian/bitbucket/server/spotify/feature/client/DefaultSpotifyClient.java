package com.atlassian.bitbucket.server.spotify.feature.client;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;
import javax.ws.rs.core.MediaType;

import com.atlassian.bitbucket.server.spotify.feature.client.model.SpotifyModel;
import com.atlassian.bitbucket.server.spotify.feature.config.ConfigurationManager;
import com.atlassian.fugue.Either;
import com.atlassian.bitbucket.server.spotify.feature.client.model.Album;
import com.atlassian.bitbucket.server.spotify.feature.client.model.Artist;
import com.atlassian.bitbucket.server.spotify.feature.client.model.Track;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.PoolingClientConnectionManager;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * {@inheritDoc}
 */
public class DefaultSpotifyClient implements SpotifyClient
{
    // We're just rendering a drop-down, too many results will make it look crappy. Plus, how many different songs are
    // called "Stairway to Heaven", amirite? It should be easy for the user to refine the search to get the right
    // result, rather than having to implement paging support.
    private static final int MAX_SEARCH_RESULTS = 10;

    private static final String VARIOUS_ARTISTS_URI = "spotify:artist:0LyfQWJT6nXafLPZqxe9Of";

    private final HttpClient httpClient;
    private final ConfigurationManager configurationManager;

    public DefaultSpotifyClient(final ConfigurationManager configurationManager)
    {
        this.configurationManager = configurationManager;
        httpClient = new DefaultHttpClient(new PoolingClientConnectionManager()); // pooling client connection manager gives us a multi-threaded HTTP client.
    }

    // Even though this endpoint can be used to look up any kind of entity, we're only ever going to use it to lookup track information, so let's assume that for now.
    @Override
    public Either<String, SpotifyModel> lookup(String spotifyId) throws JSONException, IOException
    {
        HttpGet get = new HttpGet("https://ws.spotify.com/lookup/1/.json?uri=" + URLEncoder.encode(spotifyId, "utf-8"));
        get.addHeader("accept", MediaType.APPLICATION_JSON);
        HttpEntity entity = null;
        JSONObject jsonEntity = null;
        try
        {
            HttpResponse response = httpClient.execute(get, new BasicHttpContext());
            entity = response.getEntity();
            if (response.getStatusLine().getStatusCode() != 200)
            {
                return Either.left(response.getStatusLine().getStatusCode() + ": " + EntityUtils.toString(entity));
            }
            jsonEntity = new JSONObject(EntityUtils.toString(entity));

        }
        finally
        {
            EntityUtils.consume(entity);
        }

        SpotifyModel track = jsonToTrack(jsonEntity.getJSONObject("track"));
        return Either.right(track);
    }

    private Album jsonToAlbum(JSONObject json, @Nullable String territories) throws JSONException
    {
        if (StringUtils.isBlank(territories))
        {
            territories = json.getJSONObject("availability").getString("territories");
        }

        String[] territoriesActual = territories.split(" ");

        final String albumName = json.getString("name");
        final String albumHref = json.getString("href");
        return new Album(albumHref, albumName, territoriesActual);
    }

    private Artist jsonToArtist(JSONObject artist) throws JSONException
    {
        final String artistName = artist.getString("name");
        final String artistHref = artist.has("href") ? artist.getString("href") : VARIOUS_ARTISTS_URI;

        return new Artist(artistHref, artistName);
    }

    private Track jsonToTrack(JSONObject trackJson) throws JSONException
    {
        JSONObject albumJson = trackJson.getJSONObject("album");

        // Sometimes the territory availability is defined on the track, sometimes on the album, depending on which API
        // call is made. *confused*
        // Let's retrieve it from where-ever it's defined and then always attach it to the Album model, for consistency's
        // sake.
        final Album album;
        if (trackJson.has("availability"))
        {
            String territories = trackJson.getJSONObject("availability").getString("territories");
            album = jsonToAlbum(albumJson, territories);
        }
        else
        {
            album = jsonToAlbum(albumJson, null);
        }

        JSONArray artistsJson = trackJson.getJSONArray("artists");
        ArrayList<Artist> artists = new ArrayList<Artist>(artistsJson.length());

        for (int k = 0; k < artistsJson.length(); k++)
        {
            JSONObject artistJson = artistsJson.getJSONObject(k);
            artists.add(jsonToArtist(artistJson));
        }

        final String trackName = trackJson.getString("name");
        final String trackHref = trackJson.getString("href");

        return new Track(trackHref, trackName, album, artists.toArray(new Artist[artists.size()]));
    }

    @Override
    public Either<String, Iterable<Track>> search(String query) throws JSONException, IOException
    {
        // Search Spotify
        HttpGet get = new HttpGet("https://ws.spotify.com/search/1/track.json?q=" + URLEncoder.encode(query, "utf-8"));
        get.addHeader("accept", MediaType.APPLICATION_JSON);
        HttpEntity entity = null;
        JSONObject jsonEntity = null;
        try
        {
            HttpResponse response = httpClient.execute(get, new BasicHttpContext());
            entity = response.getEntity();
            if (response.getStatusLine().getStatusCode() != 200)
            {
                return Either.left(response.getStatusLine().getStatusCode() + ": " + EntityUtils.toString(entity));
            }

            jsonEntity = new JSONObject(EntityUtils.toString(entity));
        }
        finally
        {
            if (entity != null)
            {
                EntityUtils.consume(entity);
            }
        }

        // Handle the results
        JSONArray tracks = jsonEntity.getJSONArray("tracks");

        List<Track> trackModel = new ArrayList<Track>(tracks.length());
        for (int i = 0; i < tracks.length(); i++)
        {
            JSONObject trackJson = tracks.getJSONObject(i);
            trackModel.add(jsonToTrack(trackJson));
        }

        // Spotify returns up to 100 results in a single page, throw away the results that we're not going to bother
        // displaying.
        Iterable<Track> result = Iterables.limit(trackModel, MAX_SEARCH_RESULTS);

        // Different tracks are only available in certain countries. Only show tracks available in the configured
        // territory.
        result = Iterables.filter(result, new ConfiguredTerritoryFilter(configurationManager.getTerritory()));
        return Either.right(result);
    }

    private class ConfiguredTerritoryFilter implements Predicate<Track>
    {
        private final String territory;

        public ConfiguredTerritoryFilter(final String territory)
        {
            this.territory = territory;
        }

        @Override
        public boolean apply(@Nullable Track track)
        {
            return track != null && track.getAlbum().isValidTerritory(this.territory);
        }
    }

}
