define("spotify/widget/track-selector", [
    'aui',
    'jquery',
    'bitbucket/internal/widget/searchable-multi-selector'
], function(
    AJS,
    $,
    SearchableMultiSelector) {

        function TrackMultiSelector($field, options) {
            SearchableMultiSelector.call(this, $field, options);
        }

        $.extend(true, TrackMultiSelector.prototype, SearchableMultiSelector.prototype, {
            defaults: {
                url: AJS.contextPath() + "/rest/spotify/1.0/track",
                selectionTemplate: function(track) {
                    return com.atlassian.bitbucket.server.spotify.selectedTrack({track: track});
                },
                resultTemplate: function(track) {
                    return com.atlassian.bitbucket.server.spotify.trackSelector({track: track, contextPath: AJS.contextPath()});
                },
                generateId: function(track) {
                    return track.href;
                }
            }
        });

        return TrackMultiSelector;
    }
);